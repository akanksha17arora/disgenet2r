 # disgenet2r

`disgenet2r` is an R package to query and expand DisGeNET data (www.disgenet.org), and to visualize the results within R framework.
The disgenet2r is designed to query data for DisGeNET v6.0 (Jan, 2019).
## What is this repository for?

This report is used for package distribution and testing until it is ready to be published in BioConductor.

## Package' Status

 * __Version__: 0.99.0
 * __Subversion__: `20190315`
 * __Authors__:  IBI group
 * __Maintainer__: <support@disgenet.org>

## How to start

### Installation

The package, `disgenet2r` can be installed using `devtools` from this repository:

```R
library(devtools)
install_bitbucket("ibi_group/disgenet2r")
```

### Querying DisGeNET:

The following lines show two examples of how DisGeNET can be queried using `disgenet2r`:

 * __Gene Query__

```R
library(disgenet2r)
gq <- gene2disease(gene = 3953, 
    database = "ALL", 
    score = c( 0.1,1)
)
```

 * __Disease Query__

```R
library(disgenet2r)
dq <- disease2gene(disease = "C0028754", 
    database = "ALL",
    score = c(0.3,1) 
)
```

A detailed documentation of the functions of the package is available at http://www.disgenet.org/disgenet2r.
